/*
 * This file is part of Lux Vacuos API
 * 
 * Copyright (C) 2017 Lux Vacuos
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package net.luxvacuos.api;

import net.luxvacuos.api.v1.services.Api;
import net.luxvacuos.api.v1.services.Auth;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class API {

	public static final String HOST = "https://api.luxvacuos.net/";

	public static final Auth auth;
	public static final Api api;

	static {
		Retrofit remoteAPI = new Retrofit.Builder().baseUrl(HOST).addConverterFactory(GsonConverterFactory.create())
				.build();
		auth = remoteAPI.create(Auth.class);
		api = remoteAPI.create(Api.class);
	}

}
